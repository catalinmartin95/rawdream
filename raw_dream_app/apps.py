from django.apps import AppConfig


class RawDreamAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'raw_dream_app'
